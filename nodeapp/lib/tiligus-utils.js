const tcpPortUsed = require('tcp-port-used');
const http=require('http');
const { exit } = require('process');

module.exports={
    check_empty_ports: async function (from, to) {
        return new Promise(async function (resolve, reject) {
            for (from = from; from <= to; from++) {
                await tcpPortUsed.check(from, '127.0.0.1')
                    .then(function (inUse) {
                        if (!inUse) return resolve(from), from=to+1;
                        if (from==to && inUse) return resolve(false)
                    }, function (err) {
                        console.error('Error on check:', err.message);
                    });
            }
        });
    },
    create_proxy: async function(proxyable_port, show_token){
        return new Promise(async function (resolve, reject) {
        const options = {
            hostname: '127.0.0.1',
            port: 8181,
            path: '/',
            method: 'POST',
            timeout: 2000
        }
        var all_data = ""
        const req = http.request(options, res => {

            res.on('data', d => {
                all_data += d
            })

            res.on('end', function () {
                try {
                    if(all_data.substr(0,2)=="ok"){
                        if (show_token) console.log("<proxytoken>"+all_data.substr(2)+"</proxytoken>")
                        resolve(all_data.substr(2))
                    }else{
                        return console.log("Tiligus panel service error"), process.exit(1)
                    }
                } catch (error) {
                    return console.log("tiligus-utils error"), process.exit(1)
                }
            })
        })

        req.on('error', error => {
            console.error(error)
            return console.log("Tiligus panel service error"), process.exit(1)
        })

        req.on('timeout', function(){
            return console.log("Tiligus panel service not responding"), process.exit(1)
        })

        req.write(JSON.stringify({mkproxy:proxyable_port}))
        req.end()
        })
    },
    close_proxy: function (proxy_id, exit_app){
        const options = {
            hostname: '127.0.0.1',
            port: 8181,
            path: '/',
            method: 'POST',
            timeout: 2000
        }
        var all_data = ""
        const req = http.request(options, res => {

            res.on('data', d => {
                all_data += d
            })
/*
            res.on('end', function () {
                
            })*/
        })

        req.on('error', error => {
            console.error(error)
            return console.log("Tiligus panel service error")
        })

        req.on('timeout', function(){
            return console.log("Tiligus panel service not responding")
        })

        req.write(JSON.stringify({rmproxy:proxy_id}))
        req.end()
        if(exit_app){
            setTimeout(function(){
                process.exit(1)
            }, 2100)
        }
    }
}