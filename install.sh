#!/bin/bash
tiligus_vip_wgui=/home/ubuntu/.scripts/vip-handler/remove.sh
node=/home/ubuntu/.nvm/versions/node/v14.15.5/bin/node

if test -f "$tiligus_vip_wgui"; then
    echo "Tiligus VIP is already installed." && exit
fi

sudo bash nginx_builder_install.sh # install nginx builder
rm /etc/nginx/builder/tiligus_vip.conf
echo "
location /tiligus_vip_api/ {
        rewrite /tiligus_vip_api/(.*) /$1  break;
        proxy_redirect     off;
        proxy_pass http://127.0.0.1:52000/;
        proxy_pass_request_headers on;
}" | sudo tee -a /etc/nginx/builder/tiligus_vip.conf # create API config for webserver
sudo /etc/nginx/builder/make_config.sh # build new nginx config
sudo mkdir /home/ubuntu/.scripts/vip-handler && sudo chmod 777 /home/ubuntu/.scripts/vip-handler

mv www/ /home/ubuntu/.scripts/vip-handler/www/
mv nodeapp/ /home/ubuntu/.scripts/vip-handler/nodeapp/
mv content/remove.sh $tiligus_vip_wgui

cp content/tiligus-vip.tar /home/ubuntu/.scripts/vip-handler
cd /home/ubuntu/.scripts/vip-handler
tar -xvf tiligus-vip.tar
rm tiligus-vip.tar
chmod +x vip.sh

KKL=`cat /home/ubuntu/.scripts/ubuntu_startup.sh | grep "screen -dmS tiligus-vip /home/ubuntu/.scripts/vip-handler/vip.sh"`

if [ "$KKL" == "" ]; then
        echo "Startup link don't exists, creating..."
        echo "screen -dmS tiligus-vip /home/ubuntu/.scripts/vip-handler/vip.sh" >> /home/ubuntu/.scripts/ubuntu_startup.sh ## start VIP handler service after reboot
else
        echo "Startup link already exists"
fi

curl -H "Content-type: application/json" -d '{"add_record":true, "name":"Tiligus VIP 1.0","remove_script":"/home/ubuntu/.scripts/vip-handler/remove.sh"}' 'http://127.0.0.1:8181' # add plugin to panel

cd /home/ubuntu/.scripts/vip-handler/nodeapp/
screen -dmS tiligus-vip /home/ubuntu/.scripts/vip-handler/vip.sh
$node app.js